import json

#* Creamos o importamos la lista
lenguajes = ['Python', 'Java', 'C++', 'PHP']

#* Usamos json.dumps para convertir
datos_json = json.dumps(lenguajes)

#* Imprimimos el json con los datos de la lista
print(datos_json)
print(type(datos_json))