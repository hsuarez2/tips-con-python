from datetime import datetime

ahora = datetime.now()
print(f'Primera Forma: \nFecha y Hora: {ahora}\n')

#Fecha y hora formateada
print('Segunda Forma: \nFecha y Hora: ' + ahora.strftime('%d/%m/%Y %H:%M:%S'))
print('')
print('Fecha: '+ahora.strftime('%d-%m-%Y'))
print('Hora: '+ahora.strftime('%H:%M:%S'))