import youtube_dl
#* pip install youtube_dl
#? En consola colocar esto => youtube-dl -f best url
#? En consola colocar esto => youtube-dl -f bestvideo+bestaudio url
#? En consola colocar esto => youtube-dl -f "best[height<=480]" url
#? En consola colocar esto => youtube-dl -f "bestvideo[height<=480]+bestaudio/best[height<=480]" url

def descargarVideos(link):
    diccionario = {
        # 'format': 'bestvideo[height>=720]', #? Descarga solo el video con resulción 720
        # 'format': 'bestvideo[height>=1080]', #? Descarga solo el video con resulción 1080
        'format': 'best', #? Descarga el video con audio
        'quiet': False,
    }
    with youtube_dl.YoutubeDL(diccionario) as dic:
        dic.download([link])
    print(f'Se descargo el enlace video correctamente!!!')

lista = [
        'https://www.youtube.com/watch?v=CWPo5SC3zik', #? YouTube
        'https://fb.watch/eiiyi8xngQ/' #? Facebook
        ]

for i in lista:
    descargarVideos(i)