from pdf2docx import parse
#* pip install pdf2docx

#* Definimos las rutas de entradad y salida de los archivos
pdf_file = 'documento_pdf.pdf'
dock_file = 'documento_word.docx'

#* Convierte el pdf a docx
#* start = donde comienza a convertir las paginas
#* end = donde termina de convertir las paginas
parse(pdf_file, dock_file, start=0, end=10)