import tkinter
import customtkinter
import smtplib
import ssl
from email.message import EmailMessage
import info

app = customtkinter.CTk()

app.geometry("400x240+450+200")
app.title('Mail App')

customtkinter.set_appearance_mode('dark')
customtkinter.set_default_color_theme('dark-blue')

enviar_email_a = customtkinter.CTkEntry(
    master=app,
    placeholder_text='ENVIAR EL EMAIL A',
    width=240,
    height=25,
    border_width=2,
    corner_radius=10
)
enviar_email_a.place(relx=0.5, rely=0.25, anchor=tkinter.CENTER)

asunto = customtkinter.CTkEntry(
    master=app,
    placeholder_text='ASUNTO',
    width=240,
    height=25,
    border_width=2,
    corner_radius=10
)
asunto.place(relx=0.5, rely=0.4, anchor=tkinter.CENTER)

content = customtkinter.CTkEntry(
    master=app,
    placeholder_text='CONTENIDO',
    width=240,
    height=25,
    border_width=2,
    corner_radius=10    
)
content.place(relx=0.5, rely=0.55, anchor=tkinter.CENTER)

def button_function():
    mensaje = EmailMessage()
    mensaje['From'] = info.my_email
    mensaje['To'] = str(enviar_email_a.get())
    mensaje['Subject'] = str(asunto.get())

    html = f'''
        <html>
            <body>
                <h1>{ asunto }</h1>
                <p>{ content.get() }</p>
            </body>
        </html>
    '''

    mensaje.add_alternative(html, subtype="html")
    context = ssl.create_default_context()
    
    with smtplib.SMTP_SSL('smtp.gmail.com', 465, context=context) as server:
        server.login(info.my_email, info.password)
        server.sendmail(info.my_email, str(enviar_email_a.get()), mensaje.as_string())

button = customtkinter.CTkButton(master=app, text='ENVIAR EMAIL', command=button_function)
button.place(relx=0.5, rely=0.7, anchor=tkinter.CENTER)

app.mainloop()