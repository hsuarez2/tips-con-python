
#* 'w' o 'a' para cualquiera de los 2 exista o no el archivo se creara uno nuevo
#* w => Agregar información en el archivo, esto puedo reemplazar toda la información del archivo.
#* a => Agregar información o texto sin reemplazar lo que ya habia en el archivo.
#* r => Leer el contenido de un archivo.

#* Guardamos información
#? archivo = open('../prueba.txt', 'a')
#? archivo.write('hola mundo\n')

#* Cerramos y guardamos los cambios
#? archivo.close()

#* Leemos información
#? archivo = open('../prueba.txt', 'r')
#? leer = archivo.read()
#? leer = archivo.readlines() -> Devuelve una lista por cada linea
#? print(leer)
#? archivo.close()

#* Importamos la libreria os
import os

#* Comprobamos si el archivo existe
#? existe = os.path.exists("../prueba.txt")
#? print(existe) #? -> True o False

#* Eliminamos el archivo
#? try:
#?     os.remove('../prueba.txt')
#? except FileNotFoundError as e:
#?     print("El archivo no existe")

#* Creamos una carpeta
#? try:
#?     os.mkdir(r'C:\Users\hanssel\Desktop\carpeta')
#? except FileExistsError as e:
#?     print('La carpeta ya existe')

#* Eliminamos una carpeta
#? try:
#?     os.removedirs('../carpeta')
#? except FileNotFoundError as e:
#?     print('La carpeta no existe')

#* Comprobamos si la carpeta existe
#? existe = os.path.exists("../carpeta")
#? print(existe) #? -> True o False

#* PRIMERA FORMA
#* Obtener la ruta de trabajo o directorio en donde estamos
#? print(os.getcwd())

#* SEGUNDA FORMA
#* Importamos Path de pathlib
from pathlib import Path

#* Obtenemos la ruta de trabajo
#? print('Tipo de valor: ', type(Path.cwd()))
#? print(Path.cwd())

#* Mostrar las carpetas o archivos que tenemos en este directorio
#? print(os.listdir()) #? Muestra los archivo del directorio en el que te encuentras

#? Muestra los archivo de un directorio en especifico
#? listdir() => Devuelve una lista con los archivos que encuentra
#? print(os.listdir(path=r'C:\Users\hanssel\Desktop\Facebook'))

#* Crear carpeta con Path
#* exist_ok=Valor => Evita que el codigo se rompa, se indica que la carpeta ya puede existir
#? Path(r'../carpe').mkdir(exist_ok=True)

#* Retroceder la ruta
ruta = '\\'.join(os.getcwd().split('\\')[:3])
print(ruta)