import sounddevice as sd
from scipy.io.wavfile import write
#* pip install sounddevice
#* pip install scipy

fs = 44100  #* Frecuencia de muestreo
seconds = 5  #* Duración de la grabación

myrecording = sd.rec(int(seconds * fs), samplerate=fs, channels=2)

#* Esperamos a que la grabación termine
sd.wait()

#* Guardamos la grabación
write('grabar_audio.mp3', fs, myrecording)
