from pytube import Playlist

#* https://www.youtube.com/playlist?list=PLCKuOXG0bPi10iAvvsp-uDWDOM-wRAXJm
url = input('Ingrese url de la lista de reproducción: ')

playlist = Playlist(url)

for video in playlist.videos:
    print('Titulo: ' + video.title)
    print('URL: ' + video.watch_url)
    print('Descripción: ' + video.description)
    print()