import requests
from pprint import pprint

#* URL que devuelva una respuesta en json
url = "https://www.neutrinoapi.com/apis.json"
#* Guardamos en una variable lo que obtenemos del json
resp = requests.get(url).json()

#* Imprimimos la respuesta con print()
print('Imprimimos con print()')
print(resp)

print()

#* Imprimimos la respuesta con pprint()
pprint('Imprimimos con pprint()')
pprint(resp)