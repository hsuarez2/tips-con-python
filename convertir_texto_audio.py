from gtts import gTTS
from playsound import playsound

#* pip install gTTS
#* pip install playsound==1.2.2

#* Verificamos los idiomas disponibles
#* En consola -> gtts-cli --all
#? af: Afrikaans
#? ar: Arabic
#? bg: Bulgarian
#? bn: Bengali
#? bs: Bosnian
#? ca: Catalan
#? cs: Czech
#? cy: Welsh
#? da: Danish
#? de: German
#? el: Greek
#? en: English
#? eo: Esperanto
#? es: Spanish
#? et: Estonian
#? fi: Finnish
#? fr: French
#? gu: Gujarati
#? hi: Hindi
#? hr: Croatian
#? hu: Hungarian
#? hy: Armenian
#? id: Indonesian
#? is: Icelandic
#? it: Italian
#? iw: Hebrew
#? ja: Japanese
#? jw: Javanese
#? km: Khmer
#? kn: Kannada
#? ko: Korean
#? la: Latin
#? lv: Latvian
#? mk: Macedonian
#? ml: Malayalam
#? mr: Marathi
#? ms: Malay
#? my: Myanmar (Burmese)
#? ne: Nepali
#? nl: Dutch
#? no: Norwegian
#? pl: Polish
#? pt: Portuguese
#? ro: Romanian
#? ru: Russian
#? si: Sinhala
#? sk: Slovak
#? sq: Albanian
#? sr: Serbian
#? su: Sundanese
#? sv: Swedish
#? ta: Tamil
#? te: Telugu
#? th: Thai
#? tl: Filipino
#? tr: Turkish
#? uk: Ukrainian
#? ur: Urdu
#? vi: Vietnamese
#? zh-CN: Chinese
#? zh-TW: Chinese (Mandarin/Taiwan)
#? zh: Chinese (Mandarin)

#* PRIMERA FORMA
#* gTTS('Texto a ingresar', lang = 'el tipo de pronuciación')
#? tts = gTTS('Hola mundo.Soy un texto', lang = 'es-us')

#* Guardar el texto en formato mp3
#? tts.save("audio.mp3")

#* SEGUNDA FORMA
#? tts = gTTS('Hola mundo. Estamos convirtiendo texto a voz con Python.', lang = 'es-us')

#* Guardar el texto en formato mp3 con with 
#? with open("audio.mp3","wb") as archivo:
#?    tts.write_to_fp(archivo)

#* TERCERA FORMA
#? tts = gTTS('Hola mundo. Estamos convirtiendo texto a voz con Python.', lang='es-us')
#? tts_ingles = gTTS('Hello world! testing tts in Python', lang='en')
#? tts_frances = gTTS('Bonsoir, Elliot', lang='fr-fr')

#* Guardar los textos en formato mp3 con with 
#? with open("3_hola_es_en_fr.mp3", "wb") as archivo:
#?    tts.write_to_fp(archivo)
#?    tts_ingles.write_to_fp(archivo)
#?    tts_frances.write_to_fp(archivo)

#* CUARTA FORMA
#* Especificamos el nombre del audio
NOMBRE_ARCHIVO = "convertir_texto_audio.mp3"

#* Especificamos el texto y la lengua
tts = gTTS('Hola mundo. Estamos convirtiendo texto a voz con Python.', lang='es-us')

#* Generamos un archivo y lo guardamos
with open(NOMBRE_ARCHIVO, "wb") as archivo:
    tts.write_to_fp(archivo)

#* Reproducimos el archivo generado
playsound(NOMBRE_ARCHIVO)
