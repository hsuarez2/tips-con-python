import smtplib
import ssl
from email.message import EmailMessage

asunto = 'Email desde Python'
body = 'Este es un email de testeo desde python!'

remitente = 'hsuarez2347@gmail.com'
receptor = 'ejemplo@gmail.com'
password = input('Ingrese clave: ')

message = EmailMessage()
message['From'] = remitente
message['To'] = receptor
message['Subject'] = asunto

html = f'''
    <html>
        <body>
            <h1>{ asunto }</h1>
            <h1>{ body }</h1>
        </body>
    </body>
'''

message.add_alternative(html, subtype='html')
context = ssl.create_default_context()
print('Email enviado!')

with smtplib.SMTP_SSL('smtp.gmail.com', 465, context=context) as server:
    server.login(remitente, password)
    server.sendmail(remitente, receptor, message.as_string())

print('Completado')