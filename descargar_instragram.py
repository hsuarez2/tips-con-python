#############################################################
#* Descargar imagenes, perfil y videos de un usuario publico:
#* instaloader profile nombre_usuario_a_descargar

#* Descargar imagenes, perfil y videos de un usuario privado que tengas agregado:
#* instaloader --login=tu_nombre_de_usuario profile nombre_usuario_a_descargar
#* Despues de esto ingresa tu contraseña, no te equivoques o saldra error en la descarga

#* Descargar con stories:
#* instaloader --stories profile nombre_usuario_a_descargar

#* Descargar videos IGTV:
#* instaloader --igtv profile nombre_usuario_a_descargar

#?###########################################################
#* import instaloader

#* test = instaloader.Instaloader()

#* Descarga la imagen del perfil por nombre de usuario
#* usuario = input('Ingrese nombre de usuario: ')
#* test.download_profile(usuario, profile_pic_only=True)
#* test.download_profile(usuario)
#* O utilize esto para descargar toda la informacion de ese usuario
#* test.download_profile(usuario)

