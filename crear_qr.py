import pyqrcode

website = 'https://www.udemy.com/course/flutter-ios-android-fernando-herrera/'

url = pyqrcode.create(website)

url.svg('img_qr.svg', scale=10)