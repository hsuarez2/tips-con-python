from pytube import YouTube #* pip install pytube
import getpass

#* NOMBRE DE USUARIO
name_user = getpass.getuser()

def descargaVidYoutube(url):
    #* YouTube(url).streams.get_audio_only().download(rf'C:\Users\{name_user}\Downloads') #? -> video.mp4
    YouTube(url).streams.first().download(rf'C:\Users\{name_user}\Downloads') #? -> video.3gpp
    print('Descarga finalizada')

#? https://www.youtube.com/watch?v=CWPo5SC3zik , https://www.youtube.com/watch?v=1NP6wUVBSAM&t=11s

url = input('Ingrese url del video: ')
descargaVidYoutube(url)

#? resoluciones = yt.streams.filter(progressive=True, file_extension='mp4').order_by("resolution").desc().first()
#? print(resoluciones)
#* Guardamos en una variable la calidad
#? calidad = str(resoluciones.resolution)
#? video = resoluciones.download(output_path=rf'C:\Users\{name_user}\Downloads')
