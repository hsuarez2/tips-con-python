from youtube_search import YoutubeSearch #* pip install youtube-search
import json

#* YoutubeSearch(Canal, max_results= maximo de resultados a devolver)
#* .to_json() => Tipo de retorno
results = YoutubeSearch('AuronPlay', max_results=3).to_json()

#* El resultado se devuelve en un string que contiene un formato JSON
#? print(results)

#* Transformamos el string a json
new_results = json.loads(results)

#* Imprimimos el primer resultado
print('************Datos del video por to_json()************')
print('Id de video: ' + new_results['videos'][0]['id'])
print('Titulo: ' + new_results['videos'][0]['title'])
print('Canal: ' + new_results['videos'][0]['channel'])
print('Duración de video: ' + new_results['videos'][0]['duration'])
print('N° de vistas: ' + new_results['videos'][0]['views'])
print('Tiempo de publicación: ' + new_results['videos'][0]['publish_time'])
print('Sufijo del Url: ' + new_results['videos'][0]['url_suffix'])


print("\n")
results = YoutubeSearch('Spreen', max_results=3).to_dict()

#* El resultado se devuelve en un diccionario
#? print(results)

#* Resultados en diccionario
print('************Datos del video por to_dict()************')
print('Id de video: ' + results[0]['id'])
print('Titulo: ' + results[0]['title'])
print('Canal: ' + results[0]['channel'])
print('Duración de video: ' + results[0]['duration'])
print('N° de vistas: ' + results[0]['views'])
print('Tiempo de publicación: ' + results[0]['publish_time'])
print('Sufijo del Url: ' + results[0]['url_suffix'])
