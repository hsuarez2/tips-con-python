from urllib.request import urlopen
from bs4 import BeautifulSoup
#* pip install BeautifulSoup

#* Ingresamos la pagina web
website = urlopen('https://www.wikipedia.org/')

#* Leemos la pagina
bs = BeautifulSoup(website.read(),"html.parser")

#* Cerramos la lectura
website.close()

#* Recorremos cada enlace de la pagina
for link in bs.find_all("a"):
    print(link.get("href"))