import PyPDF2
import pyttsx3

pdfReader = PyPDF2.PdfReader\
(open("C:\\Users\\hanssel suarez\\Downloads\\Libros\\Un Rostro en la Multitud - Stephen King.pdf", "rb"))

speaker = pyttsx3.init()

for num_page in range(len(pdfReader.pages)):
    text = pdfReader.pages[num_page].extract_text()
    speaker.say(text)
    speaker.runAndWait()

speaker.stop()