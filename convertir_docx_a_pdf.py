import getpass
from docx2pdf import convert
#* pip install docx2pdf

#* NOMBRE DE USUARIO
name_user = getpass.getuser()

#* Convertimos directamente
#? convert("documento_word.docx")

#* Podemos definir un archivo PDF de salida
#* Si no especificas la ruta se descargara automaticamente en documentos
#? convert("documento_word.docx", f"C://Users//{name_user}//Desktop//documento_pdf.pdf")
convert("documento_word.docx", f"documento_pdf.pdf")

#* O tambien covertir todo el contenido de una carpeta
#? convert("carpeta/")