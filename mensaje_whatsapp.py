from re import T
import pywhatkit
#* pip install pywhatkit

#* Usamos try-except
try:
    #* Enviamos el mensaje
    #* 1.Numero del movil receptor(quien recibira el mensaje) con el codigo del pais
    numero = input('Ingrese número de telefono: ')
    #* 2.Mensaje
    mensaje = input('Ingrese el mensaje a enviar: ')
    #* 3.Hora en formato de 24 horas
    #* 4.Minuto
    #* tab_close=True => Enviar mensaje automaticamente
    pywhatkit.sendwhatmsg(numero, mensaje, 7, 38, tab_close=True)
    print('Mensaje enviado')
except Exception as e:
    print(f'Ocurrio un error: {e}')