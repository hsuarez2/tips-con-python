from datetime import datetime

#* Obtenemos las fechas
dia_navidad = "2022-12-25"
dia_actual = datetime.now()

#* Validamos si hoy es navidad
if datetime.strftime(dia_actual, '%Y-%m-%d') == dia_navidad:
    print('Facialix te desea una feliz navidad.')
else:
    print('Aun no es navidad.')

