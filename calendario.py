import calendar

#* Solicitamos el año y mes al usuario
anio = int(input('Escriba el año: '))
mes = int(input('Escriba el mes: '))

#* Imprimimos el calendario deacuerdo al año y mes solicitados
print(calendar.month(anio, mes))
