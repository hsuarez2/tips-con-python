import os.path,time

#* Obtenemos la fecha de creación del archivo
print(f'Fecha de creación: {time.ctime(os.path.getctime("requirements.txt"))}')

#* Obtenemos la fecha de modificación del archivo
print(f'Fecha de Modificación: {time.ctime(os.path.getmtime("requirements.txt"))}')